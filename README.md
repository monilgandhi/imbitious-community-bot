## Micronaut 3.4.0 Documentation

- [User Guide](https://docs.micronaut.io/3.4.0/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.4.0/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.4.0/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

- [Shadow Gradle Plugin](https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow)
## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)


## To build
- from root folder `./gradlew build`


## To run
- from root folder `java -jar build/libs/bot-0.1.jar`
- Go to Imbitious test community and type any comamnd to see the response

## Required
- java 17

## Development environment
- Build on terminal using `./gradlew build`
- Open intelliJ IDEA community edition
- Select "Project from existing sources"
- Navigate to the root folder
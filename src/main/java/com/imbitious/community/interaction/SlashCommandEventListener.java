package com.imbitious.community.interaction;

import com.imbitious.community.workflow.PollWorkflow;
import discord4j.core.object.entity.Member;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.reactivestreams.Publisher;

import discord4j.core.event.ReactiveEventAdapter;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
@Singleton
public class SlashCommandEventListener extends ReactiveEventAdapter {

    @Inject
    private PollWorkflow pollWorkflow;


    @Override
    public Publisher<?> onChatInputInteraction(ChatInputInteractionEvent slashCommandEvent) {
        log.info("slash command was invoked {}", slashCommandEvent.getCommandName());
        return slashCommandEvent.deferReply().withEphemeral(true)
                .then(pollWorkflow.handle(slashCommandEvent, ChatInputInteractionEvent.class));
    }
}
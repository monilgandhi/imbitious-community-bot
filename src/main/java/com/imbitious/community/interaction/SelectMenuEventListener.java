package com.imbitious.community.interaction;

import com.imbitious.community.workflow.PollWorkflow;
import discord4j.core.event.ReactiveEventAdapter;
import discord4j.core.event.domain.interaction.SelectMenuInteractionEvent;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;


@Slf4j
@Singleton
public class SelectMenuEventListener extends ReactiveEventAdapter {

    @Inject
    private PollWorkflow pollWorkflow;

    public Publisher<?> onSelectMenuInteraction(SelectMenuInteractionEvent selectMenuInteractionEvent) {
        return selectMenuInteractionEvent.deferReply().withEphemeral(true).
                then(pollWorkflow.handle(selectMenuInteractionEvent, SelectMenuInteractionEvent.class));
    }
}

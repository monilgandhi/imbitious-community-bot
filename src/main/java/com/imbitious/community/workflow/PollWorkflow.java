package com.imbitious.community.workflow;

import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.event.domain.interaction.InteractionCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.core.spec.InteractionFollowupCreateSpec;
import discord4j.rest.util.Color;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Singleton
public class PollWorkflow implements Workflow {
    private static final String IMBITIOUS_COMMAND_NAME = "imbitious-community";
    private static final String CODY_COMMAND_HELP_COMMAND = "poll";


    @Override
    public <TEvent extends InteractionCreateEvent> Mono<Message> handle(InteractionCreateEvent event, Class<TEvent> clazz) {
        if(event == null) {
            throw new IllegalArgumentException("invalid arguments");
        }

        if(clazz == ChatInputInteractionEvent.class) {
            return handleChatInput(ChatInputInteractionEvent.class.cast(event));
        }

        // handle command
        return Mono.empty();
    }

    private Mono<Message> handleChatInput(ChatInputInteractionEvent event) {
        log.info("command Name is {}", event.getCommandName());
        if(!event.getCommandName().equals(IMBITIOUS_COMMAND_NAME)) {
            return Mono.empty();
        }

        log.info("command options is {}", event.getOptions().get(0).getName());
        if(!event.getOptions().get(0).getName().equals(CODY_COMMAND_HELP_COMMAND)) {
            return Mono.empty();
        }

        EmbedCreateSpec embedCreateSpec = EmbedCreateSpec.builder()
                .color(Color.GREEN)
                .title("Hi, I am Imbitious Community bot :wave:")
                .description("I am bot that fell out of sky  :parachute:  to help you with your interview preparation :memo:. Below is how I can be useful")
                .addField("/imbitious poll", "Poll for daily problems", false)
                .build();

        return event.createFollowup(InteractionFollowupCreateSpec.builder().addEmbed(embedCreateSpec).build());
    }

}

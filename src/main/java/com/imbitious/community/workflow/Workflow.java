package com.imbitious.community.workflow;

import discord4j.core.event.domain.interaction.InteractionCreateEvent;
import discord4j.core.object.entity.Message;
import reactor.core.publisher.Mono;

public interface Workflow {
    <TEvent extends InteractionCreateEvent> Mono<Message> handle(InteractionCreateEvent event, Class<TEvent> clazz);
}

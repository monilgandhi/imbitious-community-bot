package com.imbitious.community.config;

import com.google.common.base.Preconditions;
import discord4j.common.JacksonResources;
import discord4j.discordjson.json.ApplicationCommandRequest;
import discord4j.rest.RestClient;
import discord4j.rest.service.ApplicationService;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class GlobalCommandRegistrar {

    //Since this will only run once on startup, blocking is okay.
    public static void registerCommands(RestClient restClient) {
        Preconditions.checkNotNull(restClient);
        //Create an ObjectMapper that supports Discord4J classes
        final JacksonResources d4jMapper = JacksonResources.create();

        // Convenience variables for the sake of easier to read code below
        final ApplicationService applicationService = restClient.getApplicationService();
        final long applicationId = restClient.getApplicationId().block();

        //Get our commands json from resources as command data
        List<ApplicationCommandRequest> commands = new ArrayList<>();
        try {
            for (String json : getCommandsJson()) {
                ApplicationCommandRequest request = d4jMapper.getObjectMapper()
                        .readValue(json, ApplicationCommandRequest.class);

                commands.add(request); //Add to our array list
            }
        } catch(IOException ioEx) {
            throw new RuntimeException(ioEx);
        }

        /* Bulk overwrite commands. This is now idempotent, so it is safe to use this even when only 1 command
        is changed/added/removed
        */
        applicationService.bulkOverwriteGlobalApplicationCommand(applicationId, commands)
                .doOnNext(ignore -> log.debug("Successfully registered Global Commands"))
                .doOnError(e -> log.error("Failed to register global commands", e))
                .subscribe();
    }

    /* The two below methods are boilerplate that can be completely removed when using Spring Boot */

    private static List<String> getCommandsJson() throws IOException {
        //The name of the folder the commands json is in, inside our resources folder
        final String commandsFolderName = "commands/";

        //Get the folder as a resource
        URL url = GlobalCommandRegistrar.class.getClassLoader().getResource(commandsFolderName);
        log.info("URL is " + url.getPath());
        Objects.requireNonNull(url, commandsFolderName + " could not be found");

        Map<String, String> env = new HashMap<>();
        Path path = null;
        final String[] array = url.toString().split("!");
        log.info("Size of array is {}", array.length);
        if(array.length > 1) {
            final FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), env);
            path = fs.getPath(array[1]);
        } else {
            path = Paths.get(url.getPath());
        }

        //Get all the files inside this folder and return the contents of the files as a list of strings
        List<String> list = new ArrayList<>();
        //File[] files = Objects.requireNonNull(path..listFiles(), folder + " is not a directory");

        Files.walk(path, 2).forEach(p -> {
            try {
                if(!Files.isRegularFile(p)) {
                    return;
                }
                log.info("Adding file in {}", p.toString());

                list.add(getResourceFileAsString(commandsFolderName + p.getFileName().toString()));
            } catch(IOException ioEx) {
                log.error("error", ioEx);
            }
        });

        return list;
    }

    /**
     * Gets a specific resource file as String
     *
     * @param fileName The file path omitting "resources/"
     * @return The contents of the file as a String, otherwise throws an exception
     */
    private static String getResourceFileAsString(String fileName) throws IOException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try (InputStream resourceAsStream = classLoader.getResourceAsStream(fileName)) {
            if (resourceAsStream == null) return null;
            try (InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
                 BufferedReader reader = new BufferedReader(inputStreamReader)) {
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }
}

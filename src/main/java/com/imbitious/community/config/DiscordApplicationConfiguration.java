package com.imbitious.community.config;
import io.micronaut.context.annotation.ConfigurationProperties;
import lombok.Data;

@Data
@ConfigurationProperties("discord")
public class DiscordApplicationConfiguration {
    private String botToken;
    private Long verifiedRoleId;
}

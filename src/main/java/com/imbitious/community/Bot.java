package com.imbitious.community;

import com.imbitious.community.config.DiscordApplicationConfiguration;
import com.imbitious.community.config.GlobalCommandRegistrar;
import com.imbitious.community.interaction.SelectMenuEventListener;
import com.imbitious.community.interaction.SlashCommandEventListener;
import discord4j.core.DiscordClient;
import jakarta.inject.Inject;
import reactor.core.publisher.Mono;

public class Bot {
    @Inject
    private DiscordApplicationConfiguration discordConfiguration;

    @Inject
    private SlashCommandEventListener slashCommandEventListener;

    @Inject
    private SelectMenuEventListener selectMenuEventListener;


    private DiscordClient client;

    public Bot() {
    }

    public void run() {
        this.client = DiscordClient.create(discordConfiguration.getBotToken());
        client.withGateway(gatewayClient -> {
            GlobalCommandRegistrar.registerCommands(gatewayClient.getRestClient());
            Mono<Void> slashCommandInteraction = gatewayClient.on(slashCommandEventListener).then();
            Mono<Void> selectCommandInteraction = gatewayClient.on(selectMenuEventListener).then();
            return slashCommandInteraction.and(selectCommandInteraction);
        }).block();
    }

}

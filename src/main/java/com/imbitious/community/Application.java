package com.imbitious.community;

import io.micronaut.context.ApplicationContext;
import io.micronaut.runtime.Micronaut;

public class Application {

    public static void main(String[] args) {
        try(ApplicationContext applicationContext = Micronaut.build(args)
                .eagerInitSingletons(true)
                .mainClass(Application.class)
                .start()) {
            applicationContext.getBean(Bot.class).run();
        }
    }
}
